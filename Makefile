CC=g++-8
FLAGS=-O3 -std=gnu++17
LINKS=#-lcurl -lstdc++fs
EXECUTABLE=bin/run

ifeq (${DEBUG},1)
FLAGS+= -DDEBUG
endif

all: $(EXECUTABLE)

$(EXECUTABLE): obj/main.o
	$(CC) $(FLAGS) obj/main.o -o $@ $(LINKS)

obj/main.o: src/main.cpp
	$(CC) $(FLAGS) -c src/main.cpp -o $@ $(LINKS)

clean:
	rm -rf ./obj/*
