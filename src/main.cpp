#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
  uint max1 = 0, max2 = 0, number;

  while (cin >> number && number != 0)
  {
    if (number >= max1)
    {
      max2 = max1;
      max1 = number;
    }

    if (max2 < number && number < max1)
      max2 = number;
  }

  cout << max2 << endl;

  return 0;
}
